package objects;

import java.awt.Image;
import java.io.File;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;




public class Enemy extends Object {
	
	static String l = "images\\enemy_left.png";
	static String r = "images\\enemy_right.png";
	static String u = "images\\enemy_up.png";
	static String d = "images\\enemy_down.png";
	
	

    public Enemy(int x, int y) {
        super(x, y, l,  r,  u,  d);
        setHealth(100);  //set health at the start of the game
        
        SetImg(l); //doesn't matter which direction it's pointing at the start of the game
       }
    
    
    
    
        
    
    
    
    
    
    @Override
    public String toString() {   //prints status
        return "Enemy[" +
                ", x=" + x() +
                ", y=" + y() +
                ", health=" + health() +
                ", points=" + points() +
                ']';
    }
    
    

    public void updateXY(int nx, int ny) { // updates nx and ny  enemy does not move yet
        int nnx = nx() + nx;
        int nny = ny() + ny;
        setNX(nnx);
        setNY(nny);
    }
    public void move() {   // updates x and y  enemy moves next time when screen repainted
        setX(nx()+x());
        setY(ny()+y());
        setNX(0);
        setNY(0);
//        System.out.println("enemy moves");
    }
    
    
    
    

    
    
}
