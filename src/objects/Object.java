package objects;

import java.awt.Image;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import AI.Task;

public class Object {

    private final int SPACE = 20;
    private String name;
    private int health =100;
    private int nx; // new x
    private int ny; // new y
    private int x;
    private int y;
    private Image image;     //main image
    private Image explosion;     //bomb explosion image
    private boolean bomb_planted = false;
    private int bombx;
    private int bomby;
    private boolean visited = false;
    private String image_left;  //if actor moves (player or enemy) it has different images for movements
    private String image_right;
    private String image_up;
    private String image_down;
    
    private int points=0;
    private int value;
    private int timeLeft;
    
    
    
    Task task;
    

    public Object(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public Object(int x, int y, int value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }
    
    
    public Object(int x, int y, String l, String r, String u, String d) {
        this.x = x;
        this.y = y;
        this.image_left = l;
        this.image_right = r;
        this.image_up = u;
        this.image_down = d;
    }
    
    

    public void setVisitedTrue() {
    	visited = true;
    }
    
    public boolean visited() {
    	return visited;
    }
    
    
    public void setTimeLeft(int time) {
    	timeLeft = time;
    }
    
    public void minusTimeLeft() {
    	timeLeft--;
    }
    
    public int timeLeft() {
    	return timeLeft;
    }

    public Image getImage() {
        return this.image;
    }

    public Image getExplosion() {
        return this.explosion;
    }
    
    public void setExplosion(Image explosion) {
    	this.explosion = explosion;
		
	}
    
    public void setImage(Image img) {
        image = img;
    }
    
    
    public int bombX() {
    	return bombx;
    }
    public int bombY() {
    	return bomby;
    }
    
    public void setBombTrue() {
        bomb_planted = true;
        bombx = x;
        bomby = y;
    }
    
    public void setBombFalse() {
        bomb_planted = false;
    }
    
    
    public boolean getBombPlanted() {
        return bomb_planted;
    }


    public int x() {
        return this.x;
    }

    public int y() {
        return this.y;
    }
    
    public int nx() {
        return this.nx;
    }

    public int ny() {
        return this.ny;
    }
    
    public int points() {
        return this.points;
    }
    public int value() {
        return this.value;
    }
    public void setValue(int x) {
        this.value = x;
    }
    public void addPoints(int x) {
        this.points =  points +  x;
    }
    
    public int health() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
    
    public boolean isAlive() {
        return health > 0;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public void setNX(int x) {
        this.nx = x;
    }

    public void setNY(int y) {
        this.ny = y;
    }
    
    
    
    
    public void update() {
        if (task.getState() == null) {
            
            task.start(); // starts task at the begining of game
        }
        task.act(this);
    } 
    
    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
    
    
    
    
    

    public boolean isLeftCollision(Object actor) {
        if (((this.x() - SPACE) == actor.x()) &&
            (this.y() == actor.y())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isRightCollision(Object actor) {
        if (((this.x() + SPACE) == actor.x())
                && (this.y() == actor.y())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isTopCollision(Object actor) {
        if (((this.y() - SPACE) == actor.y()) &&
            (this.x() == actor.x())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isBottomCollision(Object actor) {
        if (((this.y() + SPACE) == actor.y())
                && (this.x() == actor.x())) {
            return true;
        } else {
            return false;
        }
    }
    
    public void SetImgLeft() { // updates enemy's picture based on direction its facing after moving

        SetImg(image_left);
    }
    
    public void SetImgRight() { 

        SetImg(image_right);
    }
    
    public void SetImgUp() { 
	        
	        SetImg(image_up);
	    }

    public void SetImgDown() { 

    	SetImg(image_down);
}
    
public void SetImg(String imgUrl) { // sets actor's image
        
        
    	
  		File tempFile = new File(imgUrl);
  		boolean exists = tempFile.exists();
 
  	if (exists) {

  		ImageIcon iia = new ImageIcon(imgUrl);
        Image image = iia.getImage();
        this.setImage(image);
  	}
  	
  	else {
  		JOptionPane.showMessageDialog(null, "ERROR: Image: " + imgUrl + " not found\n Please check images folder");
  		System.exit(0);
  	        
  	  }
    	
    }
    
    
}
