package objects;


import java.awt.Image;
import java.io.File;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Bomb extends Object {

    private Image image;
    
    private Image explosion;
    

    public Bomb(int x, int y, int ex) {
        super(x, y);

        setTimeLeft(ex);

        String imgUrl = "images\\bomb.png";
		File tempFile = new File(imgUrl);
		boolean exists = tempFile.exists();
		
	     	 
	if (exists) {
		
		
		ImageIcon iia = new ImageIcon(imgUrl);
        image = iia.getImage();
        this.setImage(image);
		
	}
	
	else {
		JOptionPane.showMessageDialog(null, "ERROR: Image: " + imgUrl + " not found\n Please check images folder");
		System.exit(0);
	        
	  }
	
	
	imgUrl = "images\\explosion.png";
	tempFile = new File(imgUrl);
	exists = tempFile.exists();
	
     	 
if (exists) {
	
	
	ImageIcon iia = new ImageIcon(imgUrl);
	explosion = iia.getImage();
    setExplosion(explosion);
	
}

else {
	JOptionPane.showMessageDialog(null, "ERROR: Image: " + imgUrl + " not found\n Please check images folder");
	System.exit(0);
        
  }

    }
    
    
    
    
    
    
    
    
    
}
