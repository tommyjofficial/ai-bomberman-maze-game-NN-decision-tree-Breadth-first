package objects;

import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Player extends Object {
	
	    public Player(int x, int y) {
        super(x, y);

        		
        String imgUrl = "images\\player_up.png";  //doesn't matter which direction it's pointing at the start of the game
        
        SetImg(imgUrl);
        
                
    }

	    public void SetImgLeft() { // updates player picture based on direction its facing
	        
	    	String imgUrl = "images\\player_left.png"; 
	        
	        SetImg(imgUrl);
	    }
	    
	    public void SetImgRight() { 
	        
	    	String imgUrl = "images\\player_right.png"; 
	        
	        SetImg(imgUrl);
	    }
	    
	public void SetImgUp() { 
		        
		    	String imgUrl = "images\\player_up.png";  
		        
		        SetImg(imgUrl);
		    }

	public void SetImgDown() { 
	    
		String imgUrl = "images\\player_down.png";  
	    
	    SetImg(imgUrl);
	}
	    
	    
    public void updateNXY(int nx, int ny) { // updates nx and ny  player does not move yet
        
        setNX(nx);
        setNY(ny);
    }
    public void move() {   // updates x and y  player moves next time when screen repainted
        setX(nx()+x());
        setY(ny()+y());

    }
    
    @Override
    public String toString() {   //prints status
        return "Player[" +
                ", x=" + x() +
                ", y=" + y() +
                ", health=" + health() +
                ", points=" + points() +
                ']';
    }


    
    
}
