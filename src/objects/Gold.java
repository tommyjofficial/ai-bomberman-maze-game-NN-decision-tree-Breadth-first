package objects;

import java.awt.Image;
import java.io.File;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Gold extends Object {

    private Image image;

    public Gold(int x, int y, int value) {
        super(x, y, value);


        String imgUrl = "images\\gold.png";
		File tempFile = new File(imgUrl);
		boolean exists = tempFile.exists();
		
	     	 
	if (exists) {
		
		
		ImageIcon iia = new ImageIcon(imgUrl);
        image = iia.getImage();
        this.setImage(image);
		
	}
	
	else {
		JOptionPane.showMessageDialog(null, "ERROR: Image: " + imgUrl + " not found\n Please check images folder");
		System.exit(0);
	        
	  }

    }
}
