package objects;

import java.awt.Image;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class EmptySpace extends Object {

public EmptySpace(int x, int y) {
        super(x, y);


        
        String imgUrl = "images\\empty.png";
		File tempFile = new File(imgUrl);
		boolean exists = tempFile.exists();
		
	     	 
	if (exists) {
		
		
		ImageIcon iia = new ImageIcon(imgUrl);
        Image image = iia.getImage();
        this.setImage(image);
		
	}
	
	else {
		JOptionPane.showMessageDialog(null, "ERROR: Image: " + imgUrl + " not found\n Please check images folder");
		System.exit(0);
	        
	  }
        
        
        
        
           }
}