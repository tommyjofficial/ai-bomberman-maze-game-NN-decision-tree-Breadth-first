package objects;

import java.awt.Image;
import java.io.File;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Bedrock extends Object {

    private Image image;

    public Bedrock(int x, int y) {
        super(x, y);


        String imgUrl = "images\\bedrock.png";
		File tempFile = new File(imgUrl);
		boolean exists = tempFile.exists();
		
	     	 
	if (exists) {
		
		
		ImageIcon iia = new ImageIcon(imgUrl);
        image = iia.getImage();
        this.setImage(image);
		
	}
	
	else {
		JOptionPane.showMessageDialog(null, "ERROR: Image: " + imgUrl + " not found\n Please check images folder");
		System.exit(0);
	        
	  }

    }
}
