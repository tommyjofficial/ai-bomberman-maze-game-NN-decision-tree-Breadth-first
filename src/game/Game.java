package game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import AI.Collect;
import AI.Move;
import AI.MoveToAndPlantBomb;
import AI.Task;
import AI.Task.TaskState;
import breadthFirstSearch.Maze1;
import breadthFirstSearch.ShortestPath;
import objects.Bedrock;
import objects.Bomb;
import objects.Diamond;
import objects.EmptySpace;
import objects.Enemy;
import objects.Gold;
import objects.Object;
import objects.Player;
import objects.Rock;


public class Game extends JPanel  implements ActionListener{ 

    private final int OFFSET = 30;
    private final int SPACE = 20;
    private final int DIFF = OFFSET - SPACE;
    public final static int move_size = 20;
    private final int is_left_empty = 1;
    private final int is_right_empty = 2;
    private final int is_up_empty = 3;
    private final int is_down_empty = 4;
    private final String left = "left";
    private final String right = "right";
    private final String up = "up";
    private final String down = "down";
    public final static int DIAMOND_VALUE = 150;
    public final static int GOLD_VALUE = 75;
    public int MAX_SCORE = 0;
    public final static int EXPOLSION_TIME = 1;
    public final static int BOMB_DAMAGE = 25;
    
    private final static int LEVEL_NUMBER = 0;     //change the map here
    private int DELAY = 15;
    public Timer timer;
    private long lastPressProcessed = 0;
        
    private Random rand = new Random();
    private boolean skipStep = false;
    private int key;
    private boolean key_pressed = false;
    private int[] keys = {0, 0, 0, 0};
    private Set<Integer> pressed = new HashSet<Integer>();
    private ArrayList bedrocks = new ArrayList();
    private ArrayList rocks = new ArrayList();
    private ArrayList<Bomb> bombs = new ArrayList();
    private ArrayList diamonds = new ArrayList();
    private ArrayList golds = new ArrayList();
    private ArrayList hiddenDiamonds = new ArrayList();
    private ArrayList hiddenGolds = new ArrayList();
    private ArrayList emptySpaces = new ArrayList();
    private ArrayList world = new ArrayList();
    private int goldsNumber =0;
    private int diamondsNumber=0;
    
    private Player player;
    private Enemy enemy;
    private int w = 0;
    private int h = 0;
    private int width = 0;
    private int height = 0;
    private boolean completed = false;
    
    
    private String [] levels = Levels.level;
    private String level = levels[LEVEL_NUMBER];
    
    public char [][] worldMatrix;

    public Game() {

    	addKeyListener(new TAdapter());

        setFocusable(true);
        initialiseWorld();
        
        worldMatrix = new char[500][500];
        
        
        timer = new Timer(DELAY, this);
//        timer.start();
        
    }

    public int getBoardWidth() {
        return this.w;
    }

    public int getBoardHeight() {
        return this.h;
    }
    
   
    

    public final void initialiseWorld() {
        
        int x = OFFSET;
        int y = OFFSET;
        
        Bedrock bedrock;
        Rock r;
        Diamond d;
        Gold g;
        EmptySpace s;
        
        
        int in = 0;
        char item = level.charAt(in);
        while(item != '\n') {
        	item = level.charAt(in++);
        	width++;
        	
        }
        

        for (int i = 0; i < level.length(); i++) {

        	item = level.charAt(i);

            if (item == '\n') {
            	height++;
            }
        }
        
        height++;


        for (int i = 0; i < level.length(); i++) {

        	item = level.charAt(i);

            if (item == '\n') {
                y += SPACE;
                if (this.w < x) {
                    this.w = x;
                }

                x = OFFSET;
                
            } else 
            	
            	if (item == '#') {
                bedrock = new Bedrock(x, y);
                bedrocks.add(bedrock);
                x += SPACE;
               }
            	 
                	
            
                 else if (item == '@') {
                 	
                     
                     r = new Rock(x, y);
                     rocks.add(r);
                     x += SPACE;
                    
                 }
                else if (item == 'd') {
                	
                	r = new Rock(x, y);
                    rocks.add(r);
                	
                    diamondsNumber++;
                    d = new Diamond(x, y,  DIAMOND_VALUE);
                    hiddenDiamonds.add(d);
                    x += SPACE;
                   
                }
                else if (item == 'g') {
                	         
                	r = new Rock(x, y);
                    rocks.add(r);
                	
                    goldsNumber++;
                    g = new Gold(x, y, GOLD_VALUE);
                    hiddenGolds.add(g);
                    x += SPACE;
                    
            }   else if (item == 'p') {
                player = new Player(x, y);
                
//                s = new EmptySpace(x, y); //only used to paint empty space picture
//                emptySpaces.add(s);
                
                x += SPACE;
                
            }
                else if (item == 'e') {
                    enemy = new Enemy(x, y);
                    
//                    s = new EmptySpace(x, y);
//                    emptySpaces.add(s);
                    x += SPACE;
                    
            } 
                else if (item == ' ') {
            	s = new EmptySpace(x, y);
                emptySpaces.add(s);
                x += SPACE;
                
            }

            h = y;
        }
        
        
        
        MAX_SCORE = (diamondsNumber * DIAMOND_VALUE)  + (goldsNumber * GOLD_VALUE);
        
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        buildWorld(g);
    }
    
    public void buildWorld(Graphics g) {

        g.setColor(new Color(213, 211, 187));
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        world = new ArrayList();
        world.addAll(emptySpaces);
        world.addAll(bedrocks);
        world.addAll(rocks);
        world.addAll(golds);
        world.addAll(bombs);
        world.addAll(diamonds);
        world.add(player);
        world.add(enemy);
        
        EmptySpace es = new EmptySpace(-20,-20);
        
        
        for (int i = 0; i < height-1; i++) {
        	for (int j = 0; j < width-1; j++) {
        		g.drawImage(es.getImage(), j*SPACE+OFFSET, i*SPACE+OFFSET, this);
        	}
        }
        
        
        for (int i = 0; i < world.size(); i++) {

            Object item = (Object) world.get(i);
            

            

            if ((item instanceof Player)  || (item instanceof Enemy) ) {
                g.drawImage(item.getImage(), item.x() + 2, item.y() + 2, this);
            }  
            	else if (item instanceof Bomb){
            		int b = (int) item.timeLeft();
	            		if(item.timeLeft() == -1) {
		            		g.drawImage(item.getExplosion(), item.x()-20, item.y()-20, this);	
		            		deleteBomb(item.x(), item.y());
	                			
            		}else {		
            			
            			g.drawImage(item.getImage(), item.x(), item.y(), this);
            			}
            } 
            	
            	else{
            		
                g.drawImage(item.getImage(), item.x(), item.y(), this);
            }

            

        }
        
        
        
        if (completed) {
            g.setColor(new Color(0, 0, 0));
            g.drawString("Completed", 25, 20);
        }
        
    }


    private void worldToMatrix() {
    	
    	
    	
    	Bedrock b;
    	Bomb bo;
        Diamond d;
        Gold g;
        Rock r;
        EmptySpace e;
    	
        worldMatrix = new char[height][width];
        
        for(int i = 0; i < emptySpaces.size(); i++) {  		//first empty spaces because its overlapped in someplaces by actors
    		e = (EmptySpace) emptySpaces.get(i);
    		worldMatrix[e.y()/ SPACE][e.x()/ SPACE] = ' ';
        
    	}

    	for(int i = 0; i < golds.size(); i++) {
    		g = (Gold) golds.get(i);
    		worldMatrix[(g.y()-DIFF) / SPACE][(g.x()-DIFF) / SPACE] = 'g';
    	}
    	for(int i = 0; i < diamonds.size(); i++) {
    		d = (Diamond) diamonds.get(i);
    		worldMatrix[(d.y()-DIFF) / SPACE][(d.x()-DIFF)/ SPACE] = 'd';
    	}
    	
    	worldMatrix[(enemy.y()-DIFF)/ SPACE][(enemy.x()-DIFF) / SPACE] = ' ';
    	
    	Collections.reverse(bombs);
    	
    	for(int i = 0; i < bombs.size(); i++) {
    		bo = (Bomb) bombs.get(i);
    		if(bombs.get(i).timeLeft() == 1) {
    			worldMatrix[((bo.y()-DIFF) / SPACE)][((bo.x()-DIFF)/ SPACE)] = '*';	
    		}
    		if(bombs.get(i).timeLeft() == 0) {  //is a bomb about to explode these tiles become ocupied
    			worldMatrix[((bo.y()-DIFF) / SPACE)+1][((bo.x()-DIFF)/ SPACE)+1] = '^';
    			worldMatrix[((bo.y()-DIFF) / SPACE)-1][((bo.x()-DIFF)/ SPACE)-1] = '^';
    			worldMatrix[((bo.y()-DIFF) / SPACE)+1][((bo.x()-DIFF)/ SPACE)-1] = '^';
    			worldMatrix[((bo.y()-DIFF) / SPACE)-1][((bo.x()-DIFF)/ SPACE)+1] = '^';
    			worldMatrix[((bo.y()-DIFF) / SPACE)+1][((bo.x()-DIFF)/ SPACE)] = '^';
    			worldMatrix[((bo.y()-DIFF) / SPACE)-1][((bo.x()-DIFF)/ SPACE)] = '^';
    			worldMatrix[((bo.y()-DIFF) / SPACE)][((bo.x()-DIFF)/ SPACE)+1] = '^';
    			worldMatrix[((bo.y()-DIFF) / SPACE)][((bo.x()-DIFF)/ SPACE)-1] = '^';
    			worldMatrix[((bo.y()-DIFF) / SPACE)][((bo.x()-DIFF)/ SPACE)] = '*';

    		} if(bombs.get(i).timeLeft() == -1) {  //is a bomb about to explode these tiles become ocupied
    			worldMatrix[((bo.y()-DIFF) / SPACE)+1][((bo.x()-DIFF)/ SPACE)+1] = '*';
    			worldMatrix[((bo.y()-DIFF) / SPACE)-1][((bo.x()-DIFF)/ SPACE)-1] = '*';
    			worldMatrix[((bo.y()-DIFF) / SPACE)+1][((bo.x()-DIFF)/ SPACE)-1] = '*';
    			worldMatrix[((bo.y()-DIFF) / SPACE)-1][((bo.x()-DIFF)/ SPACE)+1] = '*';
    			worldMatrix[((bo.y()-DIFF) / SPACE)+1][((bo.x()-DIFF)/ SPACE)] = '*';
    			worldMatrix[((bo.y()-DIFF) / SPACE)-1][((bo.x()-DIFF)/ SPACE)] = '*';
    			worldMatrix[((bo.y()-DIFF) / SPACE)][((bo.x()-DIFF)/ SPACE)+1] = '*';
    			worldMatrix[((bo.y()-DIFF) / SPACE)][((bo.x()-DIFF)/ SPACE)-1] = '*';
    			worldMatrix[((bo.y()-DIFF) / SPACE)][((bo.x()-DIFF)/ SPACE)] = '*';
    		}
//    		worldMatrix[((bo.y()-DIFF) / SPACE)][((bo.x()-DIFF)/ SPACE)] = '*';
    	}
    	
    	for(int i = 0; i < bedrocks.size(); i++) {
    		b = (Bedrock) bedrocks.get(i);
    		worldMatrix[(b.y()-DIFF) / SPACE][(b.x()-DIFF)/ SPACE] = '#';

    		
    	}
    	for(int i = 0; i < rocks.size(); i++) {
    		r = (Rock) rocks.get(i);
    		worldMatrix[(r.y()-DIFF) / SPACE][(r.x()-DIFF)/ SPACE] = '@';

    		
    	}
    	
    	worldMatrix[(player.y()-DIFF) / SPACE][(player.x()-DIFF)/ SPACE] = 'p';
    	
    	

//    	worldMatrix[6][14]='+';

    }
    
    
    
    
    
    ArrayList<Point> pathToDiamond(Object actor) {
    	
     	Point goal;
    	Point start = new Point ((actor.x()- DIFF)/SPACE, (actor.y()- DIFF)/SPACE);
    	Diamond s;
    	
    	boolean world [][] = new boolean[height][width];
    	
    	
    	for(int i = 0; i < height; i++) {  		
    		for(int j = 0; j < width; j++) {  		
    			
    			if(worldMatrix[i][j] == ' ') {
    				world[i][j] = false;
    			} else if (worldMatrix[i][j] == '�') {
    				world[i][j] = false;
    			} else if (worldMatrix[i][j] == '#') {
    				world[i][j] = true;
    			}else if (worldMatrix[i][j] == '@') {
    				world[i][j] = true;
    			}else if (worldMatrix[i][j] == 'd') {
    				world[i][j] = false;
    			} else if (worldMatrix[i][j] == 'g') {
    				world[i][j] = false;
    			} else if (worldMatrix[i][j] == 'e') {
    				world[i][j] = true;
    			}else if (worldMatrix[i][j] == 'p') {
    				world[i][j] = true;
//    				System.out.println("player" +world[i][j]);
//    				System.out.println(((j)*SPACE)+DIFF);
//    				System.out.println(((i)*SPACE)+DIFF);
    			}else if (worldMatrix[i][j] == '"') {
    				world[i][j] = true;
        			
    		} else if (worldMatrix[i][j] == '^') {
    			world[i][j] = false;
    		}else if (worldMatrix[i][j] == '*') {
    			world[i][j] = true;
    		}
    				
    			
    			
        	}
    	}
    	

    	world[(actor.y()- DIFF)/SPACE][ (actor.x()- DIFF)/SPACE] = false; //changes actor's spot to 0 in the map
    	
    	
//    	printBoolMatrix(world);



    	
    	final Maze1 maze = new Maze1(world);
    	
    	ArrayList <Point> Shortestpath = null;
    	
    	ArrayList <Point> path = new ArrayList <Point>();
    	
    	for(int i = 0; i < diamonds.size(); i++) {
    		s = (Diamond) diamonds.get(i);
    		
    		path = new ArrayList <Point>();
    		
    		goal = new Point ((s.x()-DIFF) / SPACE, (s.y()-DIFF) / SPACE);
    		
    		path = new ShortestPath().findPath(maze, start, goal);
    		
    		
    		
    		if(path != null ) {
    		
        		if(Shortestpath == null)
        			Shortestpath = path;
        		
        		if(path.size()-1 < Shortestpath.size()-1) {
        			
        			Shortestpath = path;
        			
        		} 
    		
    		} 
    		
//    		System.out.println("Shortest path length: " + (path.size() - 1));
//            System.out.println(maze.withPath(path));

    }

    	if(Shortestpath == null)
    	return Shortestpath;
    	    	
    	Shortestpath.remove(0); 					//remove first step because that's where the actor is at
    	
    	
    		for(int i = 0; i < Shortestpath.size(); i++) { 				//change back to to game coordinates from boolean matrix coordinates
    		
    		int x = (int) (Shortestpath.get(i).getX()*SPACE)+DIFF;
    		
    		int y = (int) (Shortestpath.get(i).getY()*SPACE)+DIFF;

    		Point p = new Point (x, y);

//    		System.out.println(x +" <---x  y---> "+ y);
    		
    		Shortestpath.set(i,p);

    	}
    	
    		
    		return Shortestpath;
			
}
    
    
    
    
ArrayList<Point> pathToGold(Object actor) {
    	
 	Point goal;
	Point start = new Point ((actor.x()- DIFF)/SPACE, (actor.y()- DIFF)/SPACE);
	Gold s;
	
	boolean world [][] = new boolean[height][width];
	
	
	for(int i = 0; i < height; i++) {  		
		for(int j = 0; j < width; j++) {  		
			
			if(worldMatrix[i][j] == ' ') {
				world[i][j] = false;
			} else if (worldMatrix[i][j] == '�') {
				world[i][j] = false;
			} else if (worldMatrix[i][j] == '#') {
				world[i][j] = true;
			}else if (worldMatrix[i][j] == '@') {
				world[i][j] = true;
			}else if (worldMatrix[i][j] == 'd') {
				world[i][j] = false;
			} else if (worldMatrix[i][j] == 'g') {
				world[i][j] = false;
			} else if (worldMatrix[i][j] == 'e') {
				world[i][j] = true;
			}else if (worldMatrix[i][j] == 'p') {
				world[i][j] = true;
//				System.out.println("player" +world[i][j]);
//				System.out.println(((j)*SPACE)+DIFF);
//				System.out.println(((i)*SPACE)+DIFF);
			}else if (worldMatrix[i][j] == '^') {
				world[i][j] = false;
    			
		} else if (worldMatrix[i][j] == '*') {
			world[i][j] = true;

		}
				
			
			
    	}
	}
	

	world[(actor.y()- DIFF)/SPACE][ (actor.x()- DIFF)/SPACE] = false; //changes actor's spot to 0 in the map
	
	
//	printBoolMatrix(world);



	
	final Maze1 maze = new Maze1(world);
	
	ArrayList <Point> Shortestpath = null;
	
	ArrayList <Point> path = new ArrayList <Point>();
	
	for(int i = 0; i < golds.size(); i++) {
		s = (Gold) golds.get(i);
		
		path = new ArrayList <Point>();
		
		goal = new Point ((s.x()-DIFF) / SPACE, (s.y()-DIFF) / SPACE);
		
		path = new ShortestPath().findPath(maze, start, goal);
		
		
		
		if(path != null ) {
		
    		if(Shortestpath == null)
    			Shortestpath = path;
    		
    		if(path.size()-1 < Shortestpath.size()-1) {
    			
    			Shortestpath = path;
    			
    		} 
		
		} 
		
//		System.out.println("Shortest path length: " + (path.size() - 1));
//        System.out.println(maze.withPath(path));

}

	if(Shortestpath == null)
	return Shortestpath;
	    	
	Shortestpath.remove(0); 					//remove first step because that's where the actor is at
	
	
		for(int i = 0; i < Shortestpath.size(); i++) { 				//change back to to game coordinates from boolean matrix coordinates
		
		int x = (int) (Shortestpath.get(i).getX()*SPACE)+DIFF;
		
		int y = (int) (Shortestpath.get(i).getY()*SPACE)+DIFF;

		Point p = new Point (x, y);

//		System.out.println(x +" <---x  y---> "+ y);
		
		Shortestpath.set(i,p);

	}
	
		
		return Shortestpath;
			
}



ArrayList<Point> pathToSafety(Object actor) {
	
 	Point goal;
	Point start = new Point ((actor.x()- DIFF)/SPACE, (actor.y()- DIFF)/SPACE);
	EmptySpace s;
	
	boolean world [][] = new boolean[height][width];
	
	
	for(int i = 0; i < height; i++) {  		
		for(int j = 0; j < width; j++) {  		
			
			if(worldMatrix[i][j] == ' ') {
				world[i][j] = false;
			} else if (worldMatrix[i][j] == '�') {
				world[i][j] = false;
			} else if (worldMatrix[i][j] == '#') {
				world[i][j] = true;
			}else if (worldMatrix[i][j] == '@') {
				world[i][j] = true;
			}else if (worldMatrix[i][j] == 'd') {
				world[i][j] = false;
			} else if (worldMatrix[i][j] == 'g') {
				world[i][j] = false;
			} else if (worldMatrix[i][j] == 'e') {
				world[i][j] = true;
			}else if (worldMatrix[i][j] == 'p') {
				world[i][j] = true;
//				System.out.println("player" +world[i][j]);
//				System.out.println(((j)*SPACE)+DIFF);
//				System.out.println(((i)*SPACE)+DIFF);
			}else if (worldMatrix[i][j] == '"') {
				world[i][j] = true;
    			
		} else if (worldMatrix[i][j] == '^') {
			world[i][j] = false;
			deleteEmptySpace((j*SPACE)+DIFF, (i*SPACE)+DIFF) ;
		}else if (worldMatrix[i][j] == '*') {
			world[i][j] = true;
			deleteEmptySpace((j*SPACE)+DIFF, (i*SPACE)+DIFF) ;
		}
				
			
			
    	}
	}
	

	world[(actor.y()- DIFF)/SPACE][ (actor.x()- DIFF)/SPACE] = false; 
	
	
//	printBoolMatrix(world);



	
	final Maze1 maze = new Maze1(world);
	
	ArrayList <Point> Shortestpath = null;
	
	ArrayList <Point> path = new ArrayList <Point>();
	
	for(int i = 0; i < emptySpaces.size(); i++) {
		s = (EmptySpace) emptySpaces.get(i);
		
		path = new ArrayList <Point>();
		
		goal = new Point ((s.x()-DIFF) / SPACE, (s.y()-DIFF) / SPACE);
		
		path = new ShortestPath().findPath(maze, start, goal);
		
		
		
		if(path != null ) {
		
    		if(Shortestpath == null)
    			Shortestpath = path;
    		
    		if(path.size()-1 < Shortestpath.size()-1) {
    			
    			Shortestpath = path;
    			
    		} 
		
		} 
	}
	
//	System.out.println("Shortest path length: " + (Shortestpath.size() - 1));
//    System.out.println(maze.withPath(Shortestpath));

	if(Shortestpath == null)
	return Shortestpath;
	    	
	Shortestpath.remove(0); 					//remove first step because that's where the actor is at
	
	
		for(int i = 0; i < Shortestpath.size(); i++) { 				//change back to to game coordinates from boolean matrix coordinates
		
		int x = (int) (Shortestpath.get(i).getX()*SPACE)+DIFF;
		
		int y = (int) (Shortestpath.get(i).getY()*SPACE)+DIFF;

		Point p = new Point (x, y);

//		System.out.println(x +" <---x  y---> "+ y);
		
		Shortestpath.set(i,p);

	}
	
		
		return Shortestpath;
		
}
    
    
  
  ArrayList<Point> pathToPlayer(Object actor) {
    	
    	Point goal;
    	Point start = new Point ((actor.x()- DIFF)/SPACE, (actor.y()- DIFF)/SPACE);
    	Player s;
    	
    	boolean world [][] = new boolean[height][width];
    	
    	
    	for(int i = 0; i < height; i++) {  		
    		for(int j = 0; j < width; j++) {  		
    			
    			if(worldMatrix[i][j] == ' ') {
    				world[i][j] = false;
    			} else if (worldMatrix[i][j] == '�') {
    				world[i][j] = false;
    			} else if (worldMatrix[i][j] == '#') {
    				world[i][j] = true;
    			}else if (worldMatrix[i][j] == '@') {
    				world[i][j] = true;
    			}else if (worldMatrix[i][j] == 'd') {
    				world[i][j] = false;
    			} else if (worldMatrix[i][j] == 'g') {
    				world[i][j] = false;
    			} else if (worldMatrix[i][j] == 'e') {
    				world[i][j] = true;
    			}else if (worldMatrix[i][j] == 'p') {
    				world[i][j] = true;
//    				System.out.println("player" +world[i][j]);
//    				System.out.println(((j)*SPACE)+DIFF);
//    				System.out.println(((i)*SPACE)+DIFF);
    			}else if (worldMatrix[i][j] == '^') {
    				world[i][j] = false;
        			
    		} else if (worldMatrix[i][j] == '*') {
    			world[i][j] = true;

    		}

        	}
    	}
    	
    
    	world[(actor.y()- DIFF)/SPACE][ (actor.x()- DIFF)/SPACE] = false; //changes actor's spot to 0 in the map
    	
    	
//    	printBoolMatrix(world);
    
   

    	
    	Maze1 maze = null ;
    	
    	ArrayList <Point> Shortestpath = null;
    	
    	ArrayList <Point> path = new ArrayList <Point>();
    	
    	
    		s = player;
    		
    		if(!s.visited()) {
    		
		    		path = new ArrayList <Point>();
		    		
		    		goal = new Point ((s.x()-DIFF) / SPACE, (s.y()-DIFF) / SPACE);
		    		
		    		world[(s.y()-DIFF) / SPACE][(s.x()-DIFF) / SPACE] = false;   //goal point must be false in boolean matrix for breadth-first search to work
		    		
		    		maze = new Maze1(world);
		    		
		    		path = new ShortestPath().findPath(maze, start, goal);

		    		
		    		
		    		
		    		
		    		
		    		if(path != null ) {
		    			
		    			// so path is not the same to the next rock as happens when standing on the same tile
		    			if( ((int) path.get(path.size()-2).getX() != actor.x() 
		    					&&  (int) path.get(path.size()-2).getX() != actor.y()) ) { 

		    			
			    			if(Shortestpath == null)
				    			Shortestpath = path;
				    		
				    		if(path.size()-1 < Shortestpath.size()-1) {
				    			
				    			Shortestpath = path;
				    		}
		    			}
		    		}
		    	
		               
            

    		}
    	
    
    	if(Shortestpath == null)
        	return Shortestpath;
    	    	
    	Shortestpath.remove(0); 					//remove first step because that's where the actor is at
    	Shortestpath.remove(Shortestpath.size()-1);  //remove the last step because we don't want the actor to step on it
    	
    	
//    	printBoolMatrix(world);
    	
    	
//    	System.out.println(maze.withPath(Shortestpath));
//		System.out.println("Shortest path length: " + (Shortestpath.size() - 1));
		
		
    	
    		for(int i = 0; i < Shortestpath.size(); i++) {   //converts back to world's coordinates
    		
			int x = (int) (Shortestpath.get(i).getX()*SPACE)+DIFF;
			
			int y = (int) (Shortestpath.get(i).getY()*SPACE)+DIFF;
	
			Point p = new Point (x, y);
	
//    		System.out.println(x +" <---x  y---> "+ y);
    		
    		Shortestpath.set(i,p);

    	}
    	
    		
			return Shortestpath;
}
  
  
  ArrayList<Point> pathToRock2(Object actor) {
  	
  	Point goal;
  	Point start = new Point ((actor.x()- DIFF)/SPACE, (actor.y()- DIFF)/SPACE);

  	
  	boolean world [][] = new boolean[height][width];
  	
  	
  	for(int i = 0; i < height; i++) {  		
  		for(int j = 0; j < width; j++) {  		
  			
  			if(worldMatrix[i][j] == ' ') {
  				world[i][j] = false;
  			} else if (worldMatrix[i][j] == '�') {
  				world[i][j] = false;
  			} else if (worldMatrix[i][j] == '#') {
  				world[i][j] = true;
  			}else if (worldMatrix[i][j] == '@') {
  				world[i][j] = true;
  			}else if (worldMatrix[i][j] == 'd') {
  				world[i][j] = false;
  			} else if (worldMatrix[i][j] == 'g') {
  				world[i][j] = false;
  			} else if (worldMatrix[i][j] == 'e') {
  				world[i][j] = true;
  			}else if (worldMatrix[i][j] == 'p') {
  				world[i][j] = true;
//  				System.out.println("player" +world[i][j]);
//  				System.out.println(((j)*SPACE)+DIFF);
//  				System.out.println(((i)*SPACE)+DIFF);
  			}else if (worldMatrix[i][j] == '^') {
  				world[i][j] = false;
      			
  		} else if (worldMatrix[i][j] == '*') {
  			world[i][j] = true;

  		}

      	}
  	}
  	
  
  	world[(actor.y()- DIFF)/SPACE][ (actor.x()- DIFF)/SPACE] = false; //changes actor's spot to 0 in the map
  	
  	
//  	printBoolMatrix(world);
  
 

  	
  	Maze1 maze = null ;
  	
  	ArrayList <Point> Shortestpath = null;
  	
  	ArrayList <Point> path = new ArrayList <Point>();
  	
  	
//  		s = player;
//  		
//  		if(!s.visited()) {
  	
  	Rock s;
  	
	  	for(int i = 0; i < rocks.size(); i++) {
			s = (Rock) rocks.get(i);
  		
		    		path = new ArrayList <Point>();
		    		
		    		goal = new Point ((s.x()-DIFF) / SPACE, (s.y()-DIFF) / SPACE);
		    		
		    		world[(s.y()-DIFF) / SPACE][(s.x()-DIFF) / SPACE] = false;   //goal point must be false in boolean matrix for breadth-first search to work
		    		
		    		maze = new Maze1(world);
		    		
		    		path = new ShortestPath().findPath(maze, start, goal);

		    		
		    		
		    		
		    		
		    		
		    		if(path != null ) {
		    			
		    			// so path is not the same to the next rock as happens when standing on the same tile
		    			if( ((int) path.get(path.size()-2).getX() != actor.x() 
		    					&&  (int) path.get(path.size()-2).getX() != actor.y()) ) { 

		    			
			    			if(Shortestpath == null)
				    			Shortestpath = path;
				    		
				    		if(path.size()-1 < Shortestpath.size()-1) {
				    			
				    			Shortestpath = path;
				    		}
		    			}
		    		}
		    	
	  	}
          

//  		}
  	
  
  	if(Shortestpath == null)
      	return Shortestpath;
  	    	
  	Shortestpath.remove(0); 					//remove first step because that's where the actor is at
  	Shortestpath.remove(Shortestpath.size()-1);  //remove the last step because we don't want the actor to step on it
  	
  	
//  	printBoolMatrix(world);
  	
  	
//  	System.out.println(maze.withPath(Shortestpath));
//		System.out.println("Shortest path length: " + (Shortestpath.size() - 1));
		
		
  	
  		for(int i = 0; i < Shortestpath.size(); i++) {   //converts back to world's coordinates
  		
			int x = (int) (Shortestpath.get(i).getX()*SPACE)+DIFF;
			
			int y = (int) (Shortestpath.get(i).getY()*SPACE)+DIFF;
	
			Point p = new Point (x, y);
	
//  		System.out.println(x +" <---x  y---> "+ y);
  		
  		Shortestpath.set(i,p);

  	}
  	
  		
			return Shortestpath;
}
  
  
  ArrayList<Point> pathToRock(Object actor) {
  	
  	Point goal;
  	Point start = new Point ((actor.x()- DIFF)/SPACE, (actor.y()- DIFF)/SPACE);
  	Rock s;
  	
  	boolean world [][] = new boolean[height][width];
  	
  	
  	for(int i = 0; i < height; i++) {  		
  		for(int j = 0; j < width; j++) {  		
  			
  			if(worldMatrix[i][j] == ' ') {
  				world[i][j] = false;
  			} else if (worldMatrix[i][j] == '�') {
  				world[i][j] = false;
  			} else if (worldMatrix[i][j] == '#') {
  				world[i][j] = true;
  			} else if (worldMatrix[i][j] == '@') {
      				world[i][j] = true;
  			} else if (worldMatrix[i][j] == 'd') {
  				world[i][j] = false;
  			} else if (worldMatrix[i][j] == 'g') {
  				world[i][j] = false;
  			} else if (worldMatrix[i][j] == 'e') {
  				world[i][j] = true;
  			}else if (worldMatrix[i][j] == 'p') {
  				world[i][j] = true;
//  				System.out.println("player" +world[i][j]);
//  				System.out.println(((j)*SPACE)+DIFF);
//  				System.out.println(((i)*SPACE)+DIFF);
  			} else if (worldMatrix[i][j] == '^') {
      				world[i][j] = false;
      			
  			} else if (worldMatrix[i][j] == '*') {
  				world[i][j] = true;

  			}

      	}
  	}
  	
  
  	world[(actor.y()- DIFF)/SPACE][ (actor.x()- DIFF)/SPACE] = false; //changes actor's spot to 0 in the map
  	
  	
//  	printBoolMatrix(world);
  
 

  	
  	Maze1 maze = null ;
  	
  	ArrayList <Point> Shortestpath = null;
  	
  	ArrayList <Point> path = new ArrayList <Point>();
  	
  	for(int i = 0; i < rocks.size(); i++) {
  		s = (Rock) rocks.get(i);
  		
  		if(!s.visited()) {
  		
		    		path = new ArrayList <Point>();
		    		
		    		goal = new Point ((s.x()-DIFF) / SPACE, (s.y()-DIFF) / SPACE);
		    		
		    		world[(s.y()-DIFF) / SPACE][(s.x()-DIFF) / SPACE] = false;   //goal point must be false in boolean matrix for breadth-first search to work
		    		
		    		maze = new Maze1(world);
		    		
		    		path = new ShortestPath().findPath(maze, start, goal);

		    		
		    		
		    		
		    		
		    		
		    		if(path != null ) {
		    			
		    			// so path is not the same to the next rock as happens when standing on the same tile
		    			if( ((int) path.get(path.size()-2).getX() != actor.x() 
		    					&&  (int) path.get(path.size()-2).getX() != actor.y()) ) { 

		    			
			    			if(Shortestpath == null)
				    			Shortestpath = path;
				    		
				    		if(path.size()-1 < Shortestpath.size()-1) {
				    			
				    			Shortestpath = path;
				    		}
		    			}
		    		}
		    	
		               
          

  		}
  	}
  
  	if(Shortestpath == null)
      	return Shortestpath;
  	    	
  	Shortestpath.remove(0); 					//remove first step because that's where the actor is at
//  	Shortestpath.remove(Shortestpath.size()-1);  //remove the last step because we don't want the actor to step on it
  	
  	
//  	printBoolMatrix(world);
  	
  	
//  	System.out.println(maze.withPath(Shortestpath));
//		System.out.println("Shortest path length: " + (Shortestpath.size() - 1));
		
		
  	
  		for(int i = 0; i < Shortestpath.size(); i++) {   //converts back to world's coordinates
  		
			int x = (int) (Shortestpath.get(i).getX()*SPACE)+DIFF;
			
			int y = (int) (Shortestpath.get(i).getY()*SPACE)+DIFF;
	
			Point p = new Point (x, y);
	
//  		System.out.println(x +" <---x  y---> "+ y);
  		
  		Shortestpath.set(i,p);

  	}
  	
  		
			return Shortestpath;
}
    
    
    
    
    
    
private void printBoolMatrix(boolean[][] world) {  //prints world boolean matrix in console
    

    	
    	for(int i = 0; i < height; i++) {  		
    		
    		for(int j = 0; j < width; j++) {  
    			if(world[i][j] == false) {
    			System.out.print(0);
    			} else if  (world[i][j] == true) {
        		System.out.print(1);
        		}
        	}
    		System.out.println();
    	}
    	
}
    
    
    private void printWorldMatrix() {  //prints world map in console
    
    	
    	
    	for(int i = 0; i < height; i++) {  		
    		for(int j = 0; j < width; j++) {  		
    			System.out.print(worldMatrix[i][j]);
    		
        	}
    		System.out.println();
    	}
	
}
    
    
    private boolean checkDiamondCollision(Object actor, int type) {

        if (type == is_left_empty) {

            for (int i = 0; i < diamonds.size(); i++) {
                Diamond diamond = (Diamond) diamonds.get(i);
                if (actor.isLeftCollision(diamond)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_right_empty) {

            for (int i = 0; i < diamonds.size(); i++) {
                Diamond diamond = (Diamond) diamonds.get(i);
                if (actor.isRightCollision(diamond)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_up_empty) {

            for (int i = 0; i < diamonds.size(); i++) {
                Diamond diamond = (Diamond) diamonds.get(i);
                if (actor.isTopCollision(diamond)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_down_empty) {

            for (int i = 0; i < diamonds.size(); i++) {
                Diamond diamond = (Diamond) diamonds.get(i);
                if (actor.isBottomCollision(diamond)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }
    
    private boolean checkGoldCollision(Object actor, int type) {

        if (type == is_left_empty) {

            for (int i = 0; i < golds.size(); i++) {
                Gold gold = (Gold) golds.get(i);
                if (actor.isLeftCollision(gold)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_right_empty) {

            for (int i = 0; i < golds.size(); i++) {
                Gold gold = (Gold) golds.get(i);
                if (actor.isRightCollision(gold)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_up_empty) {

            for (int i = 0; i < golds.size(); i++) {
                Gold gold = (Gold) golds.get(i);
                if (actor.isTopCollision(gold)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_down_empty) {

            for (int i = 0; i < golds.size(); i++) {
                Gold gold = (Gold) golds.get(i);
                if (actor.isBottomCollision(gold)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }
    
    
    private boolean checkBedrockCollision(Object actor, int type) {

        if (type == is_left_empty) {

            for (int i = 0; i < bedrocks.size(); i++) {
                Bedrock bedrock = (Bedrock) bedrocks.get(i);
                if (actor.isLeftCollision(bedrock)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_right_empty) {

            for (int i = 0; i < bedrocks.size(); i++) {
                Bedrock bedrock = (Bedrock) bedrocks.get(i);
                if (actor.isRightCollision(bedrock)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_up_empty) {

            for (int i = 0; i < bedrocks.size(); i++) {
                Bedrock bedrock = (Bedrock) bedrocks.get(i);
                if (actor.isTopCollision(bedrock)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_down_empty) {

            for (int i = 0; i < bedrocks.size(); i++) {
                Bedrock bedrock = (Bedrock) bedrocks.get(i);
                if (actor.isBottomCollision(bedrock)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }
    
    
    
    
    private boolean checkRockCollision(Object actor, int type) {

        if (type == is_left_empty) {

            for (int i = 0; i < rocks.size(); i++) {
            	Rock s = (Rock) rocks.get(i);
                if (actor.isLeftCollision(s)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_right_empty) {

            for (int i = 0; i < rocks.size(); i++) {
            	Rock s = (Rock) rocks.get(i);
                if (actor.isRightCollision(s)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_up_empty) {

            for (int i = 0; i < rocks.size(); i++) {
            	Rock s = (Rock) rocks.get(i);
                if (actor.isTopCollision(s)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_down_empty) {

            for (int i = 0; i < rocks.size(); i++) {
            	Rock s = (Rock) rocks.get(i);
                if (actor.isBottomCollision(s)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }
    
    
    private boolean checkBombCollision(Object actor, int type) {

        if (type == is_left_empty) {

            for (int i = 0; i < bombs.size(); i++) {
            	Bomb s = (Bomb) bombs.get(i);
                if (actor.isLeftCollision(s)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_right_empty) {

            for (int i = 0; i < bombs.size(); i++) {
            	Bomb s = (Bomb) bombs.get(i);
                if (actor.isRightCollision(s)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_up_empty) {

            for (int i = 0; i < bombs.size(); i++) {
            	Bomb s = (Bomb) bombs.get(i);
                if (actor.isTopCollision(s)) {
                    return true;
                }
            }
            return false;

        } else if (type == is_down_empty) {

            for (int i = 0; i < bombs.size(); i++) {
            	Bomb s = (Bomb) bombs.get(i);
                if (actor.isBottomCollision(s)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }
    
    
    private boolean checkEnemyCollision(Object actor, int type) {

        if (type == is_left_empty) {

         
                if (actor.isLeftCollision(enemy)) {
                    return true;

            }
            return false;

        } else if (type == is_right_empty) {

          
                if (actor.isRightCollision(enemy)) {
                    return true;
                
            }
            return false;

        } else if (type == is_up_empty) {

          
                
                if (actor.isTopCollision(enemy)) {
                    return true;
            }
            return false;

        } else if (type == is_down_empty) {

            
                if (actor.isBottomCollision(enemy)) {
                    return true;
                
            }
            return false;
        }
        return false;
    }

    
    
    public void playerMove() {

    	if(keys[0] == 1) {
    		
    		if (checkGoldCollision(player, is_left_empty)) {
        		player.addPoints(GOLD_VALUE);
        		deleteGold(player.x()-move_size, player.y());// - move_size because player.move() not called yet
//        		System.out.println("player_points: "+ player.points());
        	}
    		if (checkDiamondCollision(player, is_left_empty)) {
        		player.addPoints(DIAMOND_VALUE);
        		deleteDiamond(player.x()-move_size, player.y());// - move_size because player.move() not called yet
//        		System.out.println("player_points: "+ player.points());
        	} 
    		
    		player.updateNXY(-move_size, 0);
//			System.out.println(left);
			keys[0] = 0;
						
		} else
		
		if(keys[1] == 1) {
			
			if (checkGoldCollision(player, is_right_empty)) {
        		player.addPoints(GOLD_VALUE);
        		deleteGold(player.x()+move_size, player.y());// + move_size because player.move() not called yet
//        		System.out.println("player_points: "+ player.points());
        	}
    		if (checkDiamondCollision(player, is_right_empty)) {
        		player.addPoints(DIAMOND_VALUE);
        		deleteDiamond(player.x()+move_size, player.y());// + move_size because player.move() not called yet
//        		System.out.println("player_points: "+ player.points());
        	}
			player.updateNXY(move_size, 0);
//			System.out.println(right);
			keys[1] = 0;
			
		} else
		
		if(keys[2] == 1) {
			
			if (checkGoldCollision(player, is_up_empty)) {
        		player.addPoints(GOLD_VALUE);
        		deleteGold(player.x(), player.y()-move_size);// - move_size because player.move() not called yet
//        		System.out.println("player_points: "+ player.points());
        	}
    		if (checkDiamondCollision(player, is_up_empty)) {
        		player.addPoints(DIAMOND_VALUE);
        		deleteDiamond(player.x(), player.y()-move_size);// - move_size because player.move() not called yet
//        		System.out.println("player_points: "+ player.points());
        	}
			player.updateNXY(0, -move_size);
//			System.out.println(up);
			keys[2] = 0;
			
		} else
		
		if(keys[3] == 1) {
			
			if (checkGoldCollision(player, is_down_empty)) {
        		player.addPoints(GOLD_VALUE);
        		deleteGold(player.x(), player.y()+move_size);// + move_size because player.move() not called yet
//        		System.out.println("player_points: "+ player.points());
        	}
    		if (checkDiamondCollision(player, is_down_empty)) {
        		player.addPoints(DIAMOND_VALUE);
        		deleteDiamond(player.x(), player.y()+move_size);// + move_size because player.move() not called yet
//        		System.out.println("player_points: "+ player.points());
        	}
			player.updateNXY(0, move_size);
//			System.out.println(down);
			keys[3] = 0;
			
		}
    	
    	
    }
    
    
    
    
    public void addEmptySpace( int x, int y) {  //add epmty space to arraylist
    	
    	EmptySpace so = new EmptySpace(x, y);
    	emptySpaces.add(so);
		    		
    }
    
    public void deleteEmptySpace( int x, int y) {  //deletes epmty space
    	EmptySpace so;
    	int xo, yo; 	
    	for(int i= 0; i< emptySpaces.size();i++) {
    		so = (EmptySpace) emptySpaces.get(i);
    		xo = so.x();
    		yo = so.y();	
    		if(x == xo && y == yo) {				
    			emptySpaces.remove(i);							
    		}
    	}    		
    }
    
   
    
    
     boolean  deleteDiamond(int x, int y) {  //looks for diamond cube by coordinates and deletes it
    	Diamond so;
    	EmptySpace es;
    	int xo, yo;
    	
    	for(int i= 0; i< diamonds.size();i++) {
    		so = (Diamond) diamonds.get(i);
    		xo = so.x();
    		yo = so.y();
    		
    		
    		if(x == xo && y == yo) {		//removes dimoan  and  puts empty space in its place
    			
    			diamonds.remove(i);	
    			es = new EmptySpace(so.x(), so.y());
    			emptySpaces.add(es);
//    			System.out.println("diamond_removed");
    			
    			diamondsNumber--;
    			return true;
    		}
	
    	}  
    	
    	return false;
    }
    
    
    boolean deleteGold(int x, int y) {  //looks for gold cube by coordinates and deletes it
    	Gold so;
    	EmptySpace es;
    	int xo, yo;
    	
    	for(int i= 0; i< golds.size();i++) {
    		so = (Gold) golds.get(i);
    		xo = so.x();
    		yo = so.y();
    		
    		
    		if(x == xo && y == yo) {		//removes gold and  puts empty space in its place
    			
    			golds.remove(i);	
    			es = new EmptySpace(so.x(), so.y());
    			emptySpaces.add(es);
    			
    			goldsNumber--;
//    			System.out.println("gold_removed");
    			return true;
    		}
	
    	} 
    	
    	return false;
    }
    
    boolean deleteRock(int x, int y) {  //looks for gold cube by coordinates and deletes it
    	Rock so;
    	EmptySpace es;
    	int xo, yo;
    	
    	for(int i= 0; i< rocks.size();i++) {
    		so = (Rock) rocks.get(i);
    		xo = so.x();
    		yo = so.y();
    		
    		
    		if(x == xo && y == yo) {		//removes rock and  puts empty space in its place
    			
    			rocks.remove(i);					
    			es = new EmptySpace(so.x(), so.y());
    			emptySpaces.add(es);					
//    			System.out.println("rock_removed");
    			return true;
    		}
	
    	} 
    	
    	return false;
    }
    
    
    boolean deleteBomb(int x, int y) {  //looks for gold cube by coordinates and deletes it
    	Bomb so;
    	EmptySpace es;
    	int xo, yo;
    	
    	for(int i= 0; i< bombs.size();i++) {
    		so = (Bomb) bombs.get(i);
    		xo = so.x();
    		yo = so.y();
    		
    		
    		if(x == xo && y == yo) {		//removes rock and  puts empty space in its place
    			
    			bombs.remove(i);					
    			es = new EmptySpace(so.x(), so.y());
    			emptySpaces.add(es);					
//    			System.out.println("rock_removed");
    			return true;
    		}
	
    	} 
    	
    	return false;
    }
    
    
    void deleteBombs() {  //looks for gold cube by coordinates and deletes it
    	Bomb so;
    	EmptySpace es;
    	int xo, yo;
    	
    	for(int i= 0; i< bombs.size();i++) {
    		so = (Bomb) bombs.get(i);
	
    		if(so.timeLeft() == -1) {		//removes rock and  puts empty space in its place
    			
    			bombs.remove(i);					
    			es = new EmptySpace(so.x(), so.y());
    			emptySpaces.add(es);					
//    			System.out.println("rock_removed");
    		}
	
    	} 
    	
    }
    
    
    boolean setVisitedRock(int x, int y) {  //looks for rock cube by coordinates and sets it to visited
    	Rock so;
    	int xo, yo;
    	
    	for(int i= 0; i< rocks.size();i++) {
    		so = (Rock) rocks.get(i);
    		xo = so.x();
    		yo = so.y();

    		if(x == xo && y == yo) {		
    			((Object) rocks.get(i)).setVisitedTrue();				

    			return true;
    		}
	
    	} 
    	
    	return false;
    }
    
    
    
    
    
    private void plantBomb(Object actor){
    	
     	Bomb b;
    	
    	b = new Bomb(actor.bombX(), actor.bombY(), EXPOLSION_TIME);
        bombs.add(b);

        actor.setBombFalse();
        
//        System.out.println("BOMB___PLANTED");
	
    }
    
    
    private void anyBombsPlanted(){
    	
    	for (int i = 0; i < world.size(); i++) {
            Object item = (Object) world.get(i);
            if ((item instanceof Player)  ) {       	
            	if(player.getBombPlanted() == true) {
            		plantBomb(player);
            	} 
            }
            	else if (item instanceof Enemy){
            		if(enemy.getBombPlanted() == true) {
                		plantBomb(enemy);
                	} 
            	}  

        }
    	
    }
    
    
private void BombsMinus(){
    	
    	Bomb b;

    	for(int i = 0; i < bombs.size(); i++) { 		
    		b = (Bomb) bombs.get(i);

    		b.minusTimeLeft();
    		
    	}
    }
    
    private void explodeBombs(){
    	
    	Bomb b;

    	for(int i = 0; i < bombs.size(); i++) { 		
    		b = (Bomb) bombs.get(i);
    		
//    		System.out.println(b.timeLeft()+ "time left");
    		
    		if(b.timeLeft() == -1) {	
  			
	
	    		ExplosionResult(b.x()+SPACE, b.y()+SPACE);		//the 8 blocks around the bomb affected
	    		ExplosionResult(b.x()-SPACE, b.y()-SPACE);
	    		ExplosionResult(b.x()+SPACE, b.y()-SPACE);		
	    		ExplosionResult(b.x()-SPACE, b.y()+SPACE);
	    		ExplosionResult(b.x(), b.y()+SPACE);
	    		ExplosionResult(b.x(), b.y()-SPACE);
	    		ExplosionResult(b.x()+SPACE, b.y());
	    		ExplosionResult(b.x()-SPACE, b.y());
//	    		deleteBomb(b.x(),b.y());
    		}
    		
//    		b.minusTimeLeft();
    		
    	}
    }

    
    private void ExplosionResult(int x, int y){
    	
    	
    	
    	
    
    	for (int i = 0; i < world.size(); i++) {
            Object item = (Object) world.get(i);
            if(item.x() == x && item.y()  == y) {
            	
            	
            	if (item instanceof Player) {
                	player.setHealth(player.health()-BOMB_DAMAGE);
                } 
                else if (item instanceof Enemy) {
                	enemy.setHealth(enemy.health()-BOMB_DAMAGE);
                } 
		            
                else if (item instanceof Rock) {
		            		deleteRock(item.x(),item.y());
//		            		addEmptySpace(item.x(), item.y());
		            		if(isHiddenGold(item.x(), item.y()) ) {
			            		Gold d = new Gold(x, y, GOLD_VALUE);
			                    golds.add(d);
		            		} 
		            		 if(isHiddenDiamond(item.x(), item.y() )) {
			            		Diamond d = new Diamond(x, y, DIAMOND_VALUE);
			                    diamonds.add(d);
		            		}
		            		
		            } 
            }
     }

}
    
    
    boolean isHiddenGold(int x, int y) {  //checks if rock hides gold after explosion
    	Gold so;
    	int xo, yo;
    	
    	for(int i= 0; i< hiddenGolds.size();i++) {
    		so = (Gold) hiddenGolds.get(i);
    		xo = so.x();
    		yo = so.y();

    		if(x == xo && y == yo) {				
    			return true;
    		}
    	} 
    	
    	return false;
    }
    
    boolean isHiddenDiamond(int x, int y) {  //checks if rock hides gold after explosion
    	Diamond so;
    	int xo, yo;
    	
    	for(int i= 0; i< hiddenDiamonds.size();i++) {
    		so = (Diamond) hiddenDiamonds.get(i);
    		xo = so.x();
    		yo = so.y();

    		if(x == xo && y == yo) {				
    			return true;
    		}
    	} 
    	
    	return false;
    }
    
    boolean isInBombRangePhase1(int x, int y) {  //checks if in bomb explosion range
    	
    	x = coor0_1(x);
    	y = coor0_1(y);

			if (worldMatrix[y][x] == '^') {
				return true;
			}
			
			return false;
    }
    boolean isInBombRangePhase2(int x, int y) {  //checks if in bomb explosion range
    	
    	x = coor0_1(x);
    	y = coor0_1(y);

			if (worldMatrix[y][x] == '*') {
				return true;
			}
			
			return false;
    }
    
	private Point nearestEmptySpace(Object actor) {  //randomly chooses to a tile to step back from bomb explosion range

		Point [] point =  new Point[4];
		
		int qs=0;
		
		if (worldMatrix[coor0_1(actor.y())+1][coor0_1(actor.x())] == ' ') {
			point[qs] = new Point(actor.x(), actor.y()+SPACE);
			qs++;
		}
		if (worldMatrix[coor0_1(actor.y())-1][coor0_1(actor.x())] == ' ') {
			point[qs] = new Point(actor.x(), actor.y()-SPACE);
			qs++;
		}
		if (worldMatrix[coor0_1(actor.y())][coor0_1(actor.x())+1] == ' ') {
			point[qs] = new Point(actor.x()+SPACE, actor.y());
			qs++;
		}
		if (worldMatrix[coor0_1(actor.y())][coor0_1(actor.x())-1] == ' ') {
			point[qs] = new Point(actor.x()-SPACE, actor.y());
			qs++;
		}
		
		if(qs == 0) {
			return new Point(0,0);
		} else {
			Random rand = new Random();
			int num = rand.nextInt(qs);
			return point[num];
		}
}
	
	private Point nearestBombSpace(Object actor) {  //goes to bomb explosion zone where bomb timeLeft = 2; bomb won't explode yet

		Point [] point =  new Point[4];
		
		int qs=0;
		
		if (worldMatrix[coor0_1(actor.y())+1][coor0_1(actor.x())] == '^') {
			point[qs] = new Point(actor.x(), actor.y()+SPACE);
			qs++;
		}
		if (worldMatrix[coor0_1(actor.y())-1][coor0_1(actor.x())] == '^') {
			point[qs] = new Point(actor.x(), actor.y()-SPACE);
			qs++;
		}
		if (worldMatrix[coor0_1(actor.y())][coor0_1(actor.x())+1] == '^') {
			point[qs] = new Point(actor.x()+SPACE, actor.y());
			qs++;
		}
		if (worldMatrix[coor0_1(actor.y())][coor0_1(actor.x())-1] == '^') {
			point[qs] = new Point(actor.x()-SPACE, actor.y());
			qs++;
		}
		
		if(qs == 0) {
			return new Point(0,0);
		} else {
			Random rand = new Random();
			int num = rand.nextInt(qs);
			return point[num];
		}
}
	
    
	
	private int coor0_1(int x) {
	
	
		return (x-DIFF) /SPACE;
	}
    
	private int coor20(int x) {
		
		
		return (x *SPACE)+DIFF;
	}
	
	
private void moveToSafety() {
		
		
		ArrayList <Point> path = pathToSafety(enemy);
		
	    if(path !=null) {
	    	
	    		
		    		int x = enemy.x();
		    		int y = enemy.y();
	    		
				    	Task move = new Collect(path); 
				    	enemy.setTask(move); 
				    	
				    	enemy.update();
				    	
				    	deleteEmptySpace(enemy.x(), enemy.y());
				    	addEmptySpace(x, y);
				    	
		          if (deleteDiamond(enemy.x(), enemy.y())){
		        	  enemy.addPoints(DIAMOND_VALUE);
		          }
		          if (deleteGold(enemy.x(), enemy.y())){
		        	  enemy.addPoints(GOLD_VALUE);
		          }
		          
		          
		          
	    }
}
	

private void moveToGold() {
	
	ArrayList <Point> path = pathToGold(enemy);
	
    if(path !=null) {
    	
    	int x = enemy.x();
		int y = enemy.y();
    	
			    	Task move = new Collect(path); 
			    	enemy.setTask(move);
			    	enemy.update();
			    	
			    	deleteEmptySpace(enemy.x(), enemy.y());
			    	addEmptySpace(x, y);
			    
	    	
	    	
	          if (deleteDiamond(enemy.x(), enemy.y())){
	        	  enemy.addPoints(DIAMOND_VALUE);
	          }
	          if (deleteGold(enemy.x(), enemy.y())){
	        	  enemy.addPoints(GOLD_VALUE);
	          }
    	}
}

private void moveToPlayer() {
	
	ArrayList <Point> path = pathToPlayer(enemy);
	
    if(path !=null && path.size() != 0) {
    	
    	int x = enemy.x();
		int y = enemy.y();
    	
			    	Task move = new MoveToAndPlantBomb(path); 
			    	enemy.setTask(move);
			    	enemy.update();
			    	
			    	deleteEmptySpace(enemy.x(), enemy.y());
			    	addEmptySpace(x, y);
			    
	    	
	    	
	          if (deleteDiamond(enemy.x(), enemy.y())){
	        	  enemy.addPoints(DIAMOND_VALUE);
	          }
	          if (deleteGold(enemy.x(), enemy.y())){
	        	  enemy.addPoints(GOLD_VALUE);
	          }
    	}
}
	
	private void moveToDiamond() {
		
		ArrayList <Point> path = pathToDiamond(enemy);
		
	    if(path !=null) {
	    	
	    	int x = enemy.x();
    		int y = enemy.y();
	    	
				    	Task move = new Collect(path); 
				    	enemy.setTask(move);
				    	enemy.update();
				    	
				    	deleteEmptySpace(enemy.x(), enemy.y());
				    	addEmptySpace(x, y);
				    	
		    	
		    	
		          if (deleteDiamond(enemy.x(), enemy.y())){
		        	  enemy.addPoints(DIAMOND_VALUE);
		          }
		          if (deleteGold(enemy.x(), enemy.y())){
		        	  enemy.addPoints(GOLD_VALUE);
		          }
	    }
}
	    
	    private void moveToRocks() {
			
			ArrayList <Point> path = pathToRock(enemy);
			
		    if(path !=null) {
		    	
		    	System.out.println(path.size());
		    	
		    	if (path.size() > 1) {
		    	
				    	int x = enemy.x();
			    		int y = enemy.y();
		    	
					    	Task move = new Collect(path); 
					    	enemy.setTask(move);
					    	enemy.update();
					    	
					    	deleteEmptySpace(enemy.x(), enemy.y());
					    	addEmptySpace(x, y);
					    	
//					          if (move.getState() == Task.TaskState.Success){
//				  	
//					        	  if(setVisitedRock((int) path.get(path.size()-1).getX()+SPACE, (int) path.get(path.size()-1).getY()+SPACE) );
//					        	  if(setVisitedRock((int) path.get(path.size()-1).getX()-SPACE, (int) path.get(path.size()-1).getY()-SPACE) );
//					        	  if(setVisitedRock((int) path.get(path.size()-1).getX()+SPACE, (int) path.get(path.size()-1).getY()-SPACE) );
//					        	  if(setVisitedRock((int) path.get(path.size()-1).getX()-SPACE, (int) path.get(path.size()-1).getY()+SPACE) );
//					        	  if(setVisitedRock((int) path.get(path.size()-1).getX()+SPACE, (int) path.get(path.size()-1).getY()) );
//					        	  if(setVisitedRock((int) path.get(path.size()-1).getX()-SPACE, (int) path.get(path.size()-1).getY()) );
//					        	  if(setVisitedRock((int) path.get(path.size()-1).getX(), (int) path.get(path.size()-1).getY()+SPACE) );
//					        	  if(setVisitedRock((int) path.get(path.size()-1).getX(), (int) path.get(path.size()-1).getY()-SPACE) );
//					        	  
//				      	}
			    	
			    	
			          if (deleteDiamond(enemy.x(), enemy.y())){
			        	  enemy.addPoints(DIAMOND_VALUE);
			          }
			          if (deleteGold(enemy.x(), enemy.y())){
			        	  enemy.addPoints(GOLD_VALUE);
			          }
		    } 
		    	else {
		    		enemy.setBombTrue();
		    	}
		    	
		    	
		    	
		    
		}
		
	}
	private void moveToBombSpace() {
		Point point = nearestBombSpace(enemy);
//		System.out.println(point);
		int x= (int) point.getX();
		int y= (int)  point.getY();
		Task move = new Move(x, y); 
    	enemy.setTask(move); 
        
    		addEmptySpace(player.x()-move_size, player.y());
	}
	private void moveToEmptySpace() {
		Point point = nearestEmptySpace(enemy);
//		System.out.println(point);
		int x= (int) point.getX();
		int y= (int)  point.getY();
		Task move = new Move(x, y); 
    	enemy.setTask(move); 
        
        addEmptySpace(player.x()-move_size, player.y());
	}
	
	private void getDiamond() {
		
    	if(isInBombRangePhase1(enemy.x(), enemy.y()) ){
			ArrayList <Point> path = pathToSafety(enemy);
			if(path.size() == 0) {	
			}
    		if(path.size() > 1) {
    			
    			moveToSafety();
    		} else {
    			
    			path = pathToDiamond(enemy);
    			if(!isInBombRangePhase2((int) path.get(0).getX(), (int) path.get(0).getY())){
        			moveToDiamond();
    				}
    		}
		}
		else if (isInBombRangePhase2(enemy.x(), enemy.y()) ) {	
    		moveToSafety();
	    	} else {
    			moveToDiamond();
    			}

	}
	
	private void getGold() {
		
    	if(isInBombRangePhase1(enemy.x(), enemy.y()) ){
			ArrayList <Point> path = pathToSafety(enemy);
			if(path.size() == 0) {	
			}else
    		if(path.size() > 1) {
    			
    			moveToSafety();
    		} else {
    			
    			path = pathToGold(enemy);
    			if(!isInBombRangePhase2((int) path.get(0).getX(), (int) path.get(0).getY())){
        			moveToGold();
    				}
    		}
		}
		else if (isInBombRangePhase2(enemy.x(), enemy.y()) ) {	
    		moveToSafety();
	    	} else {
	    		moveToGold();
    			}

	}
	
	private void killPlayer() {
		
    	if(isInBombRangePhase1(enemy.x(), enemy.y()) ){
			ArrayList <Point> path = pathToSafety(enemy);
			if(path.size() == 0) {
				
			}
			else	if(path.size() > 1) {
    			
    			moveToSafety();
    		} else {
    			
    			path = pathToPlayer(enemy);
    			if(path != null && path.size() != 0) {
    			if(!isInBombRangePhase2((int) path.get(0).getX(), (int) path.get(0).getY())){
        			moveToPlayer();
    				}
    			}
    		}
		}
		else if (isInBombRangePhase2(enemy.x(), enemy.y()) ) {	
    		moveToSafety();
	    	} else {
	    		moveToPlayer();
    			}

	}
private void mine() {
		
    	if(isInBombRangePhase1(enemy.x(), enemy.y()) ){
			ArrayList <Point> path = pathToSafety(enemy);
			if(path == null) {
			} else if(path.size() == 0) {	
			}
			else	if(path.size() > 1) {
    			
    			moveToSafety();
    		} else {
    			
    			path = pathToPlayer(enemy);
    			if(path != null && path.size() != 0) {
    			if(!isInBombRangePhase2((int) path.get(0).getX(), (int) path.get(0).getY())){
        			moveToRocks();
    				}
    			}
    		}
		}
		else if (isInBombRangePhase2(enemy.x(), enemy.y()) ) {	
    		moveToSafety();
	    	} else {
	    		moveToRocks();
    			}

	}

private int PlayerRangeToDiamond() {
	ArrayList <Point> path = pathToDiamond(player);
	if(path == null)
		return -1;
	return path.size();
}
private int PlayerRangeToGold() {
	ArrayList <Point> path = pathToGold(player);
	if(path == null)
		return -1;
	return path.size();
}	
private int EnemyRangeToDiamond() {
	ArrayList <Point> path = pathToDiamond(enemy);
	if(path == null)
		return -1;
	return path.size();
}
private int EnemyRangeToGold() {
	ArrayList <Point> path = pathToGold(enemy);
	if(path == null)
		return -1;
	return path.size();
}

private int RangeToPlayer() {
	ArrayList <Point> path = pathToPlayer(enemy);
	if(path == null)
		return -1;
	return path.size();
}
private int RangeToRock() {
	ArrayList <Point> path = pathToRock(enemy);
	if(path == null)
		return -1;
	return path.size();
}

void gameOver() {
	
	
	if(!player.isAlive()) {
    	JOptionPane.showMessageDialog(null, "Health: 0. You have lost.");
  		System.exit(0);
	}
	if(!enemy.isAlive()) {
    	JOptionPane.showMessageDialog(null, "You have won! Enemy dead!");
  		System.exit(0);
	}
	if(diamondsNumber==0 && goldsNumber==0) {
		if(player.points() > enemy.points()) {
			JOptionPane.showMessageDialog(null, "You have won! All gold and diamonds have been collected. \nYour points: " 
					    							+  player.points()+". Enemy points: "+enemy.points()+".");
			System.exit(0);
		} else if (player.points() == enemy.points()) {
			JOptionPane.showMessageDialog(null, "It's a tie!\nYour points: "
					+  player.points()+". Enemy points: "+enemy.points()+".");
				System.exit(0);
			} else {
			JOptionPane.showMessageDialog(null, "All gold and diamonds have been found. You have lost!\nYour points: "
							+  player.points()+". Enemy points: "+enemy.points()+".");
					System.exit(0);
			
		}
	}
	
}


public static int decideAI(double input1, double input2, double input3, double input4, double input5)
{
	
//	System.out.println("Output results");

    Main.TheBrain.SetInput(0, input1);
    Main.TheBrain.SetInput(1, input2);
    Main.TheBrain.SetInput(2, input3);
    Main.TheBrain.SetInput(3, input4);
//    Main.TheBrain.SetInput(4, input5);

    Main.TheBrain.FeedForward();
    
//    System.out.println("\n");
//	System.out.println("--------------------------------------------------------");

	double max = -1000.0;
	int index = -1000;
	for(int j=0; j<2; j++)
	{
//		System.out.print(Main.TheBrain.GetOutput(j) + "; ");
		if (max < Main.TheBrain.GetOutput(j))
			{ 
			   max = Main.TheBrain.GetOutput(j);
			   index = j;
			}		
	}
	
	//System.out.print("index : " + index);
	
//	if (index == 0)
//		System.out.print(" 0 : " + Main.TheBrain.GetOutput(index) + "; ");
//	else if (index == 1)
//	    System.out.print(" 1 : " + Main.TheBrain.GetOutput(index) + "; ");
	

//	System.out.println("\n");
	
	
	return index;
}


    @Override
    public void actionPerformed(ActionEvent e) {  //THIS IS THE MAIN GAME CYCLE!!!!!


    																	
    	System.out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
    	
    	//CHECKS IF GAME OVER:
    	gameOver();
    	

    	deleteBombs();
    	BombsMinus();
    	
    	
    	
    	worldToMatrix();			//transfers all objects of the map at this state into matrix

    	anyBombsPlanted();
    	
    	

    	playerMove();
    	addEmptySpace(player.x(), player.y());
    	player.move();
    	player.updateNXY(0, 0);
    	
    	
    	worldToMatrix();
//    	printWorldMatrix();

//    	killPlayer();
//    	mining();   	
//    	PlayerRangeToDiamond();
//    	PlayerRangeToGold();
//    	EnemyRangeToDiamond();
//    	EnemyRangeToGold();
//    	RangeToRock();
//    	RangeToPlayer();
    	
//    	getGold(); 	
//    	getDiamond(); 
    	
    	
    	//  ARTICIAL INTELIGENCE BEHAVIUOR TREE STARTS HERE!!!!!!!!!!!!!!

    	if(EnemyRangeToDiamond() != -1 &&  EnemyRangeToDiamond() <= PlayerRangeToDiamond()) { 	//if diamond available an range is smaller than player's then get diamond
    		getDiamond(); 
    	} else if(EnemyRangeToGold() != -1 &&  EnemyRangeToGold() <= PlayerRangeToGold()) { //diamond not availble but if gold available and range is smaller than player's get gold
    			getGold();
    		} else {
    													// normalising/standardising inputs here
    			double input1 = (double) enemy.health() / 100;
    			double input2 = (double) player.health() / 100;
    			double input3 = (double) enemy.points() / MAX_SCORE;
    			double input4 = (double) player.points() / MAX_SCORE;
    			double input5;
    			if(RangeToPlayer() < RangeToRock() && RangeToPlayer() != -1) {
    				input5 = 1;
    			} else {
    				input5 = 0;
    			}
    			
//    			System.out.println("enemy health "+ input1);
//    			System.out.println("player health "+input2);
//    			System.out.println("enemy score "+input3);
//    			System.out.println("player score "+input4);
//    			System.out.println("playerCloserThanRock "+input5);
    			
    			
    			int move = decideAI(input1, input2,input3,input4,input5);	// AI NEURAL NETWORK makes decision (to mine or try to kill player)
    			System.out.println("neural networks: "+move);
    			
    			if(move == 0) {
    				mine();						//go mining
    			} else if (move == 1){
    				killPlayer();				// go kill the player
    			}
    			
    		}
    	
    	
    
    	
    	
    	explodeBombs();

        System.out.println(player);
      	System.out.println(enemy); 
//      	System.out.println(enemy.getTask().getState()); 
           		   	
//      	 printWorldMatrix();
      	
      	
        repaint();

        timer.stop();
 
    }
    
   
    

void sleep(int s) {  
	
	try {
		TimeUnit.SECONDS.sleep(s);
	} catch (InterruptedException e) {
		System.out.println("ERROR: " + e.getMessage());
	}
}
    
private class TAdapter extends KeyAdapter {

           

//        @Override
//        public void keyPressed(KeyEvent e) {
//        	
//        	int key =  e.getKeyCode();
//            
//        	System.out.println(key);
//    		
//    		    		
//    		if (key == KeyEvent.VK_LEFT) {
//    
//    		   player.updateNXY(-move_size, 0);
//    		   
//    		   keys[0] = 1;
//    		   
////    		  timer.restart();
//    		  
//    		}
//    
//            if (key == KeyEvent.VK_RIGHT) {
//            	player.updateNXY(move_size, 0);
//            	 keys[1] = 1;
//            }
//    
//            if (key == KeyEvent.VK_UP) {
//            	player.updateNXY(0, -move_size);
//            	 keys[2] = 1;
//            }
//    
//            if (key == KeyEvent.VK_DOWN) {
//            	player.updateNXY(0, move_size);
//            	 keys[3] = 1;
//            }
//        	
//        }
        
        
        
        
        @Override
        public void keyReleased(KeyEvent e) {
        	
        	int key =  e.getKeyCode();

        	// player can't move so the game goes on
        	if ((checkBedrockCollision(player, is_left_empty) ||  checkEnemyCollision(player, is_left_empty)
    	        	||  checkRockCollision(player, is_left_empty)  ||  checkBombCollision(player, is_left_empty)) &&
    	        	(checkBedrockCollision(player, is_right_empty) ||  checkEnemyCollision(player, is_right_empty) 
    	        	||  checkRockCollision(player, is_right_empty)  ||  checkBombCollision(player, is_right_empty)) &&
    	        	(checkBedrockCollision(player, is_up_empty) ||  checkEnemyCollision(player, is_up_empty)
    	        	||  checkRockCollision(player, is_up_empty)  ||  checkBombCollision(player, is_up_empty))  &&
    	        	(checkBedrockCollision(player, is_down_empty) ||  checkEnemyCollision(player, is_down_empty)
    	        	||  checkRockCollision(player, is_down_empty)  ||  checkBombCollision(player, is_down_empty)) ) {
        		timer.restart();
        	}
        	
        		

	    		
	        	if (key == KeyEvent.VK_LEFT) {
	        		if (!checkBedrockCollision(player, is_left_empty) &&  !checkEnemyCollision(player, is_left_empty)
	        	&&  !checkRockCollision(player, is_left_empty)  &&  !checkBombCollision(player, is_left_empty)  ) {
		//        		  player.updateNXY(0, 0);
	        			player.SetImgLeft();
		           		  keys[0] = 1;
		           		  timer.restart();
		        	} 
	     		}
	     
	             if (key == KeyEvent.VK_RIGHT) {
	            	 if (!checkBedrockCollision(player, is_right_empty)  &&  !checkEnemyCollision(player, is_right_empty)
	            			 &&  !checkRockCollision(player, is_right_empty)  &&  !checkBombCollision(player, is_right_empty) ) {
		//             	player.updateNXY(0, 0);
	            		 player.SetImgRight();
		             	keys[1] = 1;
		             	timer.restart();
	            	 }
	             }
	     
	             if (key == KeyEvent.VK_UP) {
	            	 if (!checkBedrockCollision(player, is_up_empty)  &&  !checkEnemyCollision(player, is_up_empty)
	            			 &&  !checkRockCollision(player, is_up_empty) &&  !checkBombCollision(player, is_up_empty) ) {
		//             	player.updateNXY(0, 0);
	            		 player.SetImgUp();
		             	keys[2] = 1;
		             	timer.restart();
	            	 }
	             }
	     
	             if (key == KeyEvent.VK_DOWN) {
	            	 if (!checkBedrockCollision(player, is_down_empty)  &&  !checkEnemyCollision(player, is_down_empty)
	            			 &&  !checkRockCollision(player, is_down_empty)   &&  !checkBombCollision(player, is_down_empty) ) {
			//             	player.updateNXY(0, 0);
	            		 player.SetImgDown();
			             	keys[3] = 1;
			             	timer.restart();
	            	 }
	             }
	             
	             if (key == KeyEvent.VK_SPACE) {
	            	 
	            	 
	            	 	player.setBombTrue();
	            		 
//	            	 	timer.restart();
	            	 
	             }
	    		
	        
        }
        
        
}
    

    
	

	


 


    
}