package game;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import AI.NeuralNetwork;
import game.Game;


public final class Main extends JFrame {

    private final int OFFSET = 30;
    
    public static double	TrainingSet[][] = { 

    		//health  playerHealth score playerScore playerCloserThanRock  MINE KILL 	  
    		{1.0, 		1.0,	 	0.0,	0.0,							1,	0},
    		{0.1,	  	0.1,	 	0.0,	0.0,							1,	0}, 
    		{0.1,	  	1.0,	 	0.0,	0.0,							1,	0},	
    		{1.0,	  	0.5,	 	0.4,	0.9,							0,	1},	
    		{1.0,	  	0.1,	 	0.0,	0.5,							0,	1},	
    		{0.6,	  	0.2,	 	0.8,	0.8,							0,	1},
    		{0.2,	  	8.0,	 	0.6,	0.9,							0,	1},
    		{0.1,	  	1.0,	 	0.9,	0.9,							1,	0},
    		{0.1,	  	0.1,	 	0.9,	0.9,							1,	0}
};
    
    
    static NeuralNetwork TheBrain = new NeuralNetwork();
    
    

    public Main() {
    	
    	
    	TheBrain.Initialize(5, 3, 2);
    	TheBrain.SetLearningRate(0.2);
    	TheBrain.SetMomentum(true, 0.9);
    	TrainTheBrain();
    	
    	JOptionPane.showMessageDialog(null, "How To Play:\nThis game is a one-step-at-the-time bomberman type game.\n"
    	+ "Move with arrows and plant bomb with Space. Bombs explode after two steps.\n"
    	+ "There are four major objects in this game: Bedrock, rock, gold and diamond, as well as bomb.\n"
    	+ "Bedrock cannot be affected by bombs, rock when exploded can contain either gold, diamond or nothing.\n"
    	+ "A diamond gives "+Game.DIAMOND_VALUE+" points, a gold piece gives "+Game.GOLD_VALUE+" points.\n"
    	+ "Both the player and the enemy has 100 health points, if in range, explosion takes away "+ Game.BOMB_DAMAGE+" health points.\n"
    	+ "Bombs can be used for mining (exploding rocks) or as a weapon to kill oponents by cornering them. The enemy can do both.\n"
    	+ "Hint: The key of winning is to waste as little steps as possible.\n"
    	+ "Good Luck!");

    	
        InitUI();
        
    }

    public void InitUI() {
        Game board = new Game();
        add(board);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(board.getBoardWidth() + OFFSET,
                board.getBoardHeight() + 2*OFFSET);
        setLocationRelativeTo(null);
        setTitle("Game4");
    }


    public static void main(String[] args) {
      
        
    	EventQueue.invokeLater(new Runnable() {
            
    		   		
            @Override
            public void run() { 
            	
            	Main game = new Main();
                game.setVisible(true);                
            }
        });
        
    }
    
    
    public static void TrainTheBrain()
    {
    	int		i;
    	double	error = 1;
    	int		c = 0;
        
    	System.out.println("************Before training*******************");
    	TheBrain.DumpData();

    	while((error > 0.05) && (c<50000))
    	{
    		error = 0;
    		c++;
    		for(i=0; i<TrainingSet.length; i++)
    		{
    			TheBrain.SetInput(0, TrainingSet[i][0]);
    			TheBrain.SetInput(1, TrainingSet[i][1]);
    			TheBrain.SetInput(2, TrainingSet[i][2]);
    			TheBrain.SetInput(3, TrainingSet[i][3]);
//    			TheBrain.SetInput(4, TrainingSet[i][4]);


    			TheBrain.SetDesiredOutput(0, TrainingSet[i][4]);
    			TheBrain.SetDesiredOutput(1, TrainingSet[i][5]);


    			TheBrain.FeedForward();
    			error += TheBrain.CalculateError();
    			TheBrain.BackPropagate();

    		}
    		error = error / 14.0f;
    	}

    	//c = c * 1;
    	System.out.println("************After training*******************");
    	TheBrain.DumpData();


    }
    
    
    
    
}
