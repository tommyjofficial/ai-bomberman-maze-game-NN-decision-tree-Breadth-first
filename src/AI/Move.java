package AI;

import game.Game;
import objects.Enemy;
import objects.Object;

public class Move extends Task {

    final protected int destX;
    final protected int destY;

    public Move(int destX, int destY) {
        super();
        this.destX = destX;
        this.destY = destY;
    }

    public void reset() {
        start();
    }

    @Override
    public void act(Object actor) {
        if (isRunning()) {
//                return;
            }
            if (!isAtDestination(actor)) {
                moveActor(actor);
            }
        }
    

    private void moveActor(Object actor) {
        if (destY != actor.y()) {
            if (destY > actor.y()) {
                actor.setY(actor.y() + Game.move_size);
                actor.SetImgDown();
                
            } else {
                actor.setY(actor.y() - Game.move_size);
                actor.SetImgUp();
                
            }
        }
        if (destX != actor.x()) {
            if (destX > actor.x()) {
                actor.setX(actor.x() + Game.move_size);
                actor.SetImgRight();
                
            } else {
                actor.setX(actor.x() - Game.move_size);
                actor.SetImgLeft();
                
            }
        }
        if (isAtDestination(actor)) {
            succeed();
        }
    }
    
    


    private boolean isAtDestination(Object actor) {
        return destX == actor.x() && destY == actor.y();
    }
}