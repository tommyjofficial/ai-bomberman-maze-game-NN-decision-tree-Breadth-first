package AI;

import game.Game;
import objects.Object;

public abstract class Task {

    public enum TaskState {
        Success,
        Failure,
        Running
    }

    protected TaskState state;

    protected Task() { }

    public void start() {
        System.out.println(">>> Starting task: " + this.getClass().getSimpleName());
        this.state = TaskState.Running;
    }

    public abstract void reset();

    public abstract void act(Object actor);

    protected void succeed() {
        System.out.println(">>> Task: " + this.getClass().getSimpleName() + " SUCCEEDED");
        this.state = TaskState.Success;
    }

    protected void fail() {
        System.out.println(">>> Task: " + this.getClass().getSimpleName() + " FAILED");
        this.state = TaskState.Failure;
    }

    public boolean isSuccess() {
        return state.equals(TaskState.Success);
    }

    public boolean isFailure() {
        return state.equals(TaskState.Failure);
    }

    public boolean isRunning() {
        return state.equals(TaskState.Running);
    }

    public TaskState getState() {
        return state;
    }
}