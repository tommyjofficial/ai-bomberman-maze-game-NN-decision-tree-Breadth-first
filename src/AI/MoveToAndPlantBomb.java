package AI;


import java.awt.Point;
import java.util.ArrayList;

import game.Game;
import objects.Object;

public class MoveToAndPlantBomb extends Task {

	
	ArrayList <Point> path = null;
	
	int destX ;
	int destY ;
	
	
	    public MoveToAndPlantBomb(ArrayList <Point> p) {
	        super();
	        
	        path = p;
	        

	    }

	    public void reset() {
	        start();
	    }

	    @Override
	    public void act(Object actor) {
	    	
	    	
	    	setDestination();
	    	
	    	
	        if (isRunning()) {
//	                return;
	            }
	        
	        
	        
	        
	        
//	        System.out.println (" this s " + (int) path.get(0).getX() + (int) path.get(0).getY() );
	        
//	        System.out.println ( path.size() );
	        
	        Task moveTo = new Move( (int) path.get(0).getX(), (int) path.get(0).getY());
	        
	        actor.setTask(moveTo);
	        
	        actor.update();
	        
//	        System.out.println ( "destX " + destX + "destY "+ destY);
	    
	        if (isAtDestination(actor)) {
	        	
	        	actor.setBombTrue();
	        	
	            succeed();
	        }
	    
	    }
	    
	    private void setDestination() {
	        destX = (int) path.get(path.size()-1).getX() ;
	        destY = (int) path.get(path.size()-1).getY();
	    }
	    
	    
	    private boolean isAtDestination(Object actor) {
	        return destX == actor.x() && destY == actor.y();
	    }
	
}
