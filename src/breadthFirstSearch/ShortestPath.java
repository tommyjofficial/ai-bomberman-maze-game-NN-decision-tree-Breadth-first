package breadthFirstSearch;

import java.awt.Point;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;

public class ShortestPath {  // breadth-first search implemented

    private Maze1 maze;
    
    
    
    private Point source;
    private Point target;
    
    
    
    
    
    private boolean[][] visited;
    private Map<Point, Point> parents;
    
    

    public ShortestPath() {}

    private ShortestPath(final Maze1 maze, 
                           final Point source, 
                           
                           final Point target) {
    	
    	
    		Objects.requireNonNull(maze, "The input maze is null.");
        Objects.requireNonNull(source, "The source node is null.");
        	Objects.requireNonNull(target, "The target node is null.");

        	
        	
        	
        	
        this.maze = maze;
        this.source = source;
        
        this.target = target;

        checkSourceNode();
        
//        checkTargetNode();

        this.visited = new boolean[maze.getHeight()][maze.getWidth()];
        this.parents = new HashMap<>();
        	this.parents.put(source, null);
        	
        	
        	
    }

    
    
    public ArrayList<Point> findPath(final Maze1 maze, 
                                final Point source, 
                                final Point target) {
        return new ShortestPath(maze, source, target).compute();
        
        
    }

    
    
    private ArrayList<Point> compute() {
    	
    	
    	
    		final Queue<Point> queue = new ArrayDeque<>();
        final Map<Point, Integer> distances = new HashMap<>();

        	queue.add(source);
        distances.put(source, 0);
        

        while (!queue.isEmpty()) {
        	
            // Removes the head of the queuee.
        	
            final Point current = queue.remove();

            if (current.equals(target)) {
                return constructPath();
            }
            

            for (final Point child : generateChildren(current)) {
                if (!parents.containsKey(child)) {
                	
                    parents.put(child, current);
                    
                    
                    
                    
                    
                    
                    queue.add(child);
                }
            }
            
            
            
        }
        
        
        
        

        // null means that the target node is not reachable
        return null;
        
    }

    private ArrayList<Point> constructPath() {
        Point current = target;
        
        
        final ArrayList<Point> path = new ArrayList<>();

        while (current != null) {
            path.add(current);
            current = parents.get(current);
        }
        Collections.<Point>reverse(path);
        return path;
    }

    
    private Iterable<Point> generateChildren(final Point current) {
    	
        final Point north = new Point(current.x, current.y - 1);
        
        	final Point south = new  Point(current.x, current.y + 1);
        final Point west = new Point(current.x - 1, current.y);
        
        
        
        final Point east = new Point(current.x + 1, current.y);
        
        final ArrayList<Point> childList = new ArrayList<>(4);

        
        	if (maze.cellIsTrav(north)) {
        		childList.add(north);
        	}

        if (maze.cellIsTrav( south)) {
            childList.add(south);
        }
        
        if	 (maze.cellIsTrav (west)) {
            childList.add(west);
            
        }
        	if (maze.cellIsTrav (east)) {
        		
        		
        		childList.add(east);
        	}
        	

        return childList;
    }

    private void checkSourceNode() {
    	
        checkNode(source, 
                  	"The nodeis outside the maze.");

        
        if (!maze.cellIsFree(source.x, source.y)) {
            throw new IllegalArgumentException(
                    "occupied cell");
        }
        
    }

//    private void checkTargetNode() {
//        checkNode(target,  "is outside the maze. " );
//
//        if (!maze.cellIsFree(target.x, target.y)) {
//            throw new IllegalArgumentException(
//                    "The target occupied.");
//        }
//    }

    private void checkNode(final Point node, final String errorMessage) {
        if (node.x < 0 || node.x >= maze.getWidth()|| node.y < 0|| node.y >= maze.getHeight()) {
        	
        	
            throw new IllegalArgumentException(errorMessage);
            
        }
    }
    


}
