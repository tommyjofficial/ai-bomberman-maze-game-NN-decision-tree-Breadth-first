# AI Bomberman Maze game

This game is a one-step-at-the-time Bomberman (a popular video game) type of game. The game implements Neural Networks and an agent behaviour tree for AI decision making. Breadth-first search algorithm is used for path-finding.
 
## The goal of the game 
The goal of the game is to collect as much gold and diamonds as possible. A diamond gives 150 points, a gold piece gives 75 points. Whoever collects more points, wins. Another way to win the game is to kill the opponent using bombs. If in range, explosion takes away 25 health points from the player or the enemy. Each character has 100 health points in total, which takes four times to get affected by bomb to lose.

## Instructions
Plant bombs with Space key and move with arrow keys.    